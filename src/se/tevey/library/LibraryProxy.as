package se.tevey.library
{
	/**
	 * <b>LibraryProxy.as</b><br>
	 * Is used to load an swf library and get objects by linkagename from the loaded library.
	 * @author Viktor René (viktor.rene[at]gmail.com)
	 * */
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	public class LibraryProxy extends EventDispatcher
	{
		private static var _instance:LibraryProxy;
		
		private var _loader:Loader;
		private var _urlRequest:URLRequest;
		private var _content:MovieClip;
		private var _subscriber:ILibrarySubscriber;
		
		public function LibraryProxy(singletonEnforcer:SingletonEnforcer)
		{
			if(!_instance)
				_instance = this;
		}
		
		private static function getInstance():LibraryProxy
		{
			if(!_instance)
			{
				_instance = new LibraryProxy(new SingletonEnforcer());
			}
			return _instance;
		}
		
		/**
		 * @param libraryUrl the url String to the swf file you want to load.
		 */
		public static function loadLibrary(libraryUrl:String):void
		{
			if(!getInstance()._subscriber)
				throw new Error("Use addSubscriber to to add a subscriber");
			
			getInstance().loadLibrary(libraryUrl);
		}
		
		/**
		 * @param subscriber the Class listening for loading related events.(must implement ILibrarySubscriber)
		 */
		public static function addSubscriber(subscriber:ILibrarySubscriber):void
		{
			getInstance()._subscriber = subscriber;
		}
		
		/**
		 * @param id The linkage name of a Movieclip in the Loaded Library
		 */
		public static function getMovieClipById(id:String):MovieClip
		{
			return getInstance().getMovieClipById(id);
		}
		
		/**
		 * @param id The linkage name of a image in the Loaded Library
		 */
		public static function getBitmapDataById(id:String):BitmapData
		{
			return getInstance().getBitmapDataById(id);
		}
		
		/**
		 * @param id The linkage name of a image in the Loaded Library
		 */
		public static function getBitmapById(id:String):Bitmap
		{
			return new Bitmap(getInstance().getBitmapDataById(id));
		}
		
		/**
		 * @param id The linkage name of a image in the Loaded Library
		 */
		public static function getSpriteById(id:String):Sprite
		{
			var s:Sprite = new Sprite();
			s.addChild(getBitmapById(id));
			return s;
		}
		
		/**
		 * @param id The linkage name of a Sound in the Loaded Library
		 */
		public static function getSoundById(id:String):Sound
		{
			return getInstance().getSoundById(id);
		}
		
		private function getBitmapDataById(id:String):BitmapData
		{
			var bitmapData:Class = _loader.content.loaderInfo.applicationDomain.getDefinition(id) as Class;
			return new bitmapData();
		}
		
		private function getMovieClipById(id:String):MovieClip
		{
			var mc:Class = _loader.content.loaderInfo.applicationDomain.getDefinition(id) as Class;
			return new mc();
		}
		
		private function getSoundById(id:String):Sound
		{
			var sound:Class = _loader.content.loaderInfo.applicationDomain.getDefinition(id) as Class;
			return new sound();
		}
		
		private function loadLibrary(libraryUrl:String):void
		{
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			if(!_urlRequest)
				_urlRequest = new URLRequest();
			
			_urlRequest.url = libraryUrl;
			_loader.load(_urlRequest);
		}
		
		private function onProgress(e:ProgressEvent):void
		{
			_subscriber.libraryEvent(e);
		}
		
		private function onIOError(e:IOErrorEvent):void
		{
			_subscriber.libraryEvent(e);
		}
		
		private function onComplete(e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
			_loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_subscriber.libraryEvent(e);
		}
	}
}

class SingletonEnforcer
{
	public function SingletonEnforcer():void
	{
	}
}
