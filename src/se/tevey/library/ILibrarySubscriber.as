package se.tevey.library
{
	/**
	 * <b>ILibrarySubscriber</b><br>
	 * The Class handling the loading of libaries must implement this Class<br>
	 * and use a switch on e.type in the function libraryEvent.<br>
	 * Available events:<br>
	 * Event.COMPLETE<br>
	 * ProgressEvent.PROGRESS<br>
	 * IOErrorEvent.IO_ERROR
	 * @author Viktor René (viktor.rene[at]gmail.com)
	 * */
	import flash.events.Event;
	
	public interface ILibrarySubscriber
	{
		function libraryEvent(e:Event):void
	}
}
